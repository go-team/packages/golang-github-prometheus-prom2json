Source: golang-github-prometheus-prom2json
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Cyril Brulebois <cyril@debamax.com>
Section: golang
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
               golang-github-davecgh-go-spew-dev,
               golang-github-prometheus-client-model-dev,
               golang-github-prometheus-common-dev,
               golang-protobuf-extensions-dev
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-prometheus-prom2json
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-prometheus-prom2json.git
Homepage: https://github.com/prometheus/prom2json
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/prometheus/prom2json

Package: golang-github-prometheus-prom2json-dev
Architecture: all
Depends: golang-github-davecgh-go-spew-dev,
         golang-github-prometheus-client-model-dev,
         golang-github-prometheus-common-dev,
         golang-protobuf-extensions-dev,
         ${misc:Depends}
Multi-Arch: foreign
Description: tool to scrape a Prometheus client and dump the result as JSON (library)
 Since the deprecation of the JSON exposition format, looking into
 metrics exposed by Prometheus can be a challenge, as one would
 usually need to interact with the Prometheus server and its protocol
 buffer format. Since JSON is a wildly-supported format, prom2json was
 designed to scrape a Prometheus client in protocol buffer or text
 format, in order to make the results available in JSON format.
